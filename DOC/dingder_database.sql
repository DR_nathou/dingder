-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: mysql-dingder.alwaysdata.net
-- Generation Time: May 26, 2018 at 02:40 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dingder_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `ArtistEntity`
--

CREATE TABLE `ArtistEntity` (
  `id` varchar(50) NOT NULL,
  `mb_name` varchar(50) NOT NULL,
  `location` varchar(50) DEFAULT NULL,
  `picture` varchar(150) DEFAULT NULL,
  `youtube` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ArtistTagEntity`
--

CREATE TABLE `ArtistTagEntity` (
  `id` varchar(50) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `artist_id` varchar(50) NOT NULL,
  `tag_count` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `MatchEntity`
--

CREATE TABLE `MatchEntity` (
  `id` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_match_id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `is_ignored` tinyint(1) DEFAULT NULL,
  `is_liked` tinyint(1) DEFAULT NULL,
  `is_new` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `PreferenceEntity`
--

CREATE TABLE `PreferenceEntity` (
  `id` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `artist_id` varchar(50) DEFAULT NULL,
  `group_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `TagEntity`
--

CREATE TABLE `TagEntity` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `mb_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `UserEntity`
--

CREATE TABLE `UserEntity` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nickname` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `registration_date` datetime NOT NULL,
  `last_connection_date` datetime NOT NULL,
  `birthdate` date NOT NULL,
  `sex` varchar(15) DEFAULT 'Undetermined',
  `location` varchar(150) DEFAULT NULL,
  `picture` varchar(254) DEFAULT '/img/defaultprofileundetermined.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ArtistEntity`
--
ALTER TABLE `ArtistEntity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ArtistTagEntity`
--
ALTER TABLE `ArtistTagEntity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `MatchEntity`
--
ALTER TABLE `MatchEntity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `PreferenceEntity`
--
ALTER TABLE `PreferenceEntity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TagEntity`
--
ALTER TABLE `TagEntity`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tag` (`mb_name`);

--
-- Indexes for table `UserEntity`
--
ALTER TABLE `UserEntity`
  ADD PRIMARY KEY (`id`) USING HASH,
  ADD UNIQUE KEY `Email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `TagEntity`
--
ALTER TABLE `TagEntity`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `UserEntity`
--
ALTER TABLE `UserEntity`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
