<?php

namespace App\BU;

use Cake\Controller\Controller;
use Cake\Event\Event;

use App\Controller\AppController;
/**
 * ApiClientManager
 *
 * Connects to MusicBrainz and query de ws
 */
class ApiClientManager
{
	const MusicBrainzEndpoint = 'https://musicbrainz.org/ws/2/';
	const WikiEndPoint = 'https://en.wikipedia.org/w/api.php?action=query&titles=Image:{0}&prop=imageinfo&iiprop=url&format=json';
	const YoutubeBaseUrl = 'https://www.youtube.com/results?search_query=';
	
	function Search($query)
	{
		try
		{		
			$query = 'artist/?query=artist:'.urlencode($query).'*&fmt=json&inc=url-rels'; //&limit=10&offset=9
			//print_r(ApiClientManager::MusicBrainzEndpoint . $query);
			$curl = curl_init(ApiClientManager::MusicBrainzEndpoint . $query);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_USERAGENT,'dingder/0.1.0 (https://dingder.alwaysdata.net) (nathan.owski@gmail.com)');
			$curl_response = curl_exec($curl);
			curl_close($curl);
			
			$toreturn = json_decode($curl_response, true);//XML version simplexml_load_string($curl_response);
			
			//print_r($toreturn);
			
			if($toreturn == '') null;

			return $toreturn;
			
		}
		catch(Exception $ex)
		{
			return "ERROR type ".$ex->getMessage() . " executing query: " . $query;
		}
	}
	
	function Get($query)
	{
		try
		{
			//img is at $result['reslations'][6][type] == 'image' ? $result['reslations'][6][url][ressource]
			$query = 'artist/'.urlencode($query) . '?fmt=json&inc=url-rels+tags';
			//print_r(ApiClientManager::MusicBrainzEndpoint . $query);
			$curl = curl_init(ApiClientManager::MusicBrainzEndpoint . $query);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_USERAGENT,'dingder/0.1.0 (https://dingder.alwaysdata.net) (nathan.owski@gmail.com)');
			$curl_response = curl_exec($curl);
			curl_close($curl);
			
			$toreturn = json_decode($curl_response, true);//XML version -=> simplexml_load_string($curl_response);
			
			$albumArt = $this->RetrieveAlbumArt($toreturn);
			
			$youtube = $this->RetrieveYoutubeLink($toreturn);
			
			$toreturn['picture'] = $albumArt;
			
			$toreturn['youtube'] = $youtube;
			//print_r($toreturn);
			
			if($toreturn == '') return null;

			return $toreturn;
			
		}
		catch(Exception $ex)
		{
			return "ERROR type ".$ex->getMessage() . " executing query: " . $query;
		}
	}
	function RetrieveYoutubeLink($result)
	{
		if (array_key_exists('relations', $result))
		{
			$relations = $result['relations'];
			if (is_array($relations) || is_object($relations))
			{
		
				foreach ($relations as $rel)
				{
					if($rel['type'] == 'youtube')
						return $rel['url']['resource'];
				}
			}
		}
		return null;
	}
	
	
	
	function RetrieveAlbumArt($result)
	{
		if (array_key_exists('relations', $result))
		{
			$relations = $result['relations'];
			if (is_array($relations) || is_object($relations))
			{
		
				foreach ($relations as $rel)
				{
					if($rel['type'] == 'image')
						return $this->DownloadImage($rel['url']['resource']);
				}
			}
		}
		return "/img/record.jpg";
	}
	
	function DownloadImage($resource)
	{
		try
		{
			$query = str_replace('{0}',ltrim(substr($resource, strpos($resource, 'e:')), 'e:'), ApiClientManager::WikiEndPoint);
			
			$curl = curl_init($query);
			
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_USERAGENT,'dingder/0.1.0 (https://dingder.alwaysdata.net) (nathan.owski@gmail.com)');
			$curl_response = curl_exec($curl);
			curl_close($curl);
			
			if (isset($curl_response))
			{
				$toreturn = json_decode($curl_response, true);//XML version -=> simplexml_load_string($curl_response);
				
				foreach($toreturn['query']['pages'] as $jsonProp) 
				{
					if(array_key_exists('imageinfo', $jsonProp));
						return $jsonProp['imageinfo'][0]['url'];
				}
			}
			
			return "/img/record.jpg";
		}
		catch(Exception $ex)
		{
			return "/img/record.jpg";
		}
	}
	

	
	function Example($query)
	{
		$curl = curl_init(ApiClientManager::endpoint);
		$curl_post_data = array(
            "user_id" => 42,
            "emailaddress" => 'lorna@example.com',
            );
       curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($curl, CURLOPT_POST, true);
       curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
       $curl_response = curl_exec($curl);
       curl_close($curl);

       $xml = new SimpleXMLElement($curl_response);
	}
}


