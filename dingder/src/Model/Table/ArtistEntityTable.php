<?php 
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ArtistEntityTable extends Table
{
	public function initialize(array $config)
    {
        $this->setTable('ArtistEntity'); // your table name
		$this->setRegistryAlias('ArtistEntity');
		$this->setAlias('ArtistEntity');
		$this->hasMany('PreferenceEntity');
		$this->hasMany('ArtistTagEntity');
	}
	
}	 