<?php 
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PreferenceEntityTable extends Table
{
	public function initialize(array $config)
    {
        $this->setTable('PreferenceEntity');
		$this->setRegistryAlias('PreferenceEntity');
		$this->setAlias('PreferenceEntity');
		
		
		$this->hasMany('UserEntity');
		$this->hasMany('ArtistEntity');
		
		$this->belongsTo('UserEntity', [
                'className' => 'UserEntity'
            ])
            ->setForeignKey('user_id')
            ->setProperty('user_id')
			->setJoinType('INNER');
			
		$this->belongsTo('ArtistEntity', [
                'className' => 'ArtistEntity'
            ])
            ->setForeignKey('artist_id')
            ->setProperty('artist_id')
			->setJoinType('INNER');
		
	}
}	 