<?php 
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class TagEntityTable extends Table
{
	public function initialize(array $config)
    {
        $this->setTable('TagEntity'); // your table name
		$this->setRegistryAlias('TagEntity');
		$this->setAlias('TagEntity');
		$this->hasMany('ArtistTagEntity');
	}
	
}	 