<?php 
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class MatchEntityTable extends Table
{
	public function initialize(array $config)
    {
        $this->setTable('MatchEntity'); // MY table name
		$this->setRegistryAlias('MatchEntity');
		$this->setAlias('MatchEntity');
		
		$this->hasMany('UserEntity');
		
		$this->belongsTo('UserEntity', [
                'className' => 'UserEntity'
            ])
            ->setForeignKey('user_id')
            ->setProperty('user_id')
			->setJoinType('INNER');
			
		$this->belongsTo('UserEntity', [
			'className' => 'UserEntity'
		])
		->setForeignKey('user_match_id')
		->setProperty('user_match_id')
		->setJoinType('INNER');
	}
	
	
}	 