<?php 
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UserEntityTable extends Table
{
	public function initialize(array $config)
    {
        $this->setTable('UserEntity'); // your table name
		$this->setRegistryAlias('UserEntity');
		$this->setAlias('UserEntity');
		$this->hasMany('PreferenceEntity');
	}
	
	public function findAuth(\Cake\ORM\Query $query, array $options)
	{
		$query->select(['id', 'email', 'password']);
		return $query;
	}
	
	public function validationDefault(Validator $validator)
    {
		//$validator->add('email', 'valid-email', ['rule' => 'email']);
        return $validator
            ->notEmpty('email', 'A Email is required')
            ->notEmpty('username', 'A Email is required')
            ->notEmpty('password', 'A password is required');
    }

	public function validationHardened(Validator $validator)
	{
		$validator = $this->validationDefault($validator);

		$validator->add('Password', 'length', ['rule' => ['lengthBetween', 6, 100], 'message' => __('Password should have at least 6 characters'),]);
		return $validator;
	}
}	 