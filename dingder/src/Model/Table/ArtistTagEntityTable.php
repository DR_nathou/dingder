<?php 
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ArtistTagEntityTable extends Table
{
	public function initialize(array $config)
    {
        $this->setTable('ArtistTagEntity');
		$this->setRegistryAlias('ArtistTagEntity');
		$this->setAlias('ArtistTagEntity');
		
		
		$this->hasMany('TagEntity');
		$this->hasMany('ArtistEntity');
		
		$this->belongsTo('TagEntity', [
                'className' => 'TagEntity'
            ])
            ->setForeignKey('tag_id')
            ->setProperty('tag_id')
			->setJoinType('INNER');
			
		$this->belongsTo('ArtistEntity', [
                'className' => 'ArtistEntity'
            ])
            ->setForeignKey('artist_id')
            ->setProperty('artist_id')
			->setJoinType('INNER');
		
	}
}	 