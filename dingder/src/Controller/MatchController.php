<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use App\BU\ApiClientManager;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Model\UserEntity;
use Model\ArtistEntity;
use Model\PreferenceEntity;
use Cake\Datasource\ConnectionManager;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class MatchController extends AppController
{	

	const COLOR = [0 => '#1078B1', 20 => '#2f6197', 40 => '#4d4c7f', 60 => '#723461', 80 => '#9f173d', 100 => '#bf0223'];
	
	public function Index()
    {
		$this->redirect('/match/♥');
    }
	

	public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
		
		$this->Auth->config('authorize', false);
    }
	
	
	public function isAuthorized($user) {
        //auth check
        //return boolean
		return true;
    }
	

    public function view($id)
    {
		parent::view($id);
    }
	
	
	public function ♥()
	{
		$this->loadModel('MatchEntity');
		$this->MatchMe();
		$this->set('MatchList', $this->Get());
	}
	
	
	
	public function Like()
	{
		try
		{
			$id = $this->request->getParam('pass')[0];
			if (isset($id)) 
			{
				$matchRepository= TableRegistry::get('MatchEntity');
				
				$match = $matchRepository->get($id);
				$match->is_new = false;
				$match->is_liked = true;
				$match->is_ignored = false;
			
				$matchRepository->save($match);
			}		
		}
		catch(\PDOException $ex)
		{
			$this->Flash->error('Sorry something really bad happened, your action did not succeed');
		}
		finally
		{
			return $this->redirect('/match/♥');
		}
	}
	
	public function Ignore()
	{
		try
		{
			$id = $this->request->getParam('pass')[0];
			if (isset($id)) 
			{
				$matchRepository= TableRegistry::get('MatchEntity');
				
				$match = $matchRepository->get($id);
				$match->is_new = false;
				$match->is_liked = false;
				$match->is_ignored = true;
			
				$matchRepository->save($match);
			}		
		}
		catch(\PDOException $ex)
		{
			$this->Flash->error('Sorry something really bad happened, your action did not succeed');
		}
		finally
		{
			return $this->redirect('/match/♥');
		}
	}
	
	
	public function MatchMe()
	{
		$matchList = array();
		
		array_push($matchList, $this->MatchByArtist());
		//array_push($matchList, $this->MatchByTag());
		
		return $matchList;
	}
	
	public function GetNewMatchCount()
	{
		$count = 0;
		$matchList = $this->Get();
		foreach($matchList as $match)
		{
			if($match['is_new'] == true) $count++;
		}
		return $count;
	}
	
	public function Get()
	{
		try
		{
			$this->loadModel('MatchEntity');
			
			$allMatch = $this->MatchEntity->find('all')->contain('UserEntity', 'UserEntity')->where('user_id = \'' . ($this->Auth->user()['id']) . '\'')->hydrate(false);;
			
			return $allMatch->toArray();
			
		}
		catch(\PdoException $ex)
		{
			return array();
		}
		
	}
	//has been i missed soething and its broken now
	private function ColorMatchPercent($score)
	{
		$score = (int)$score;
				 
		if($score > 0   && $score < 20)       { var_dump('true');return MatchController::COLOR[0]  ; }
		else if($score >= 20 && $score < 40)  { return MatchController::COLOR[20] ; }
		else if($score >= 40 && $score < 60)  { return MatchController::COLOR[40] ; }
		else if($score >= 60 && $score < 80)  { return MatchController::COLOR[60] ; }
		else if($score >= 80 && $score < 90)  { return MatchController::COLOR[80] ; }
		else if($score >= 90 && $score < 100) { return MatchController::COLOR[100]; }
	}
	
	private function MatchByArtist()
	{	
				
		$conn = ConnectionManager::get('default');
				
		$stmt = $conn->execute("SELECT user_id as user_match_id, count(*) as score FROM PreferenceEntity where artist_id in (SELECT artist_id FROM PreferenceEntity where user_id = :userId) and user_id != :userId group by user_id", ['userId' => $this->Auth->user()['id']]); 
		
		$result = $stmt->fetchAll('assoc');
		
		
		//$matchList = $this->CalculateMatch($result);
		
		foreach($result as $match)
		{
			
			$this->InsertOrUpdate($match, $conn);
			
		}		
	}
	
	private function InsertOrUpdate($match, $conn)
	{
		try
		{
			
			$this->loadModel('MatchEntity');
			
			$stmt = $conn->execute("SELECT * from MatchEntity where id = ?", [$match['user_match_id'] . '-' . $this->Auth->user()['id']]);
		
			$result = $stmt->fetchAll('assoc');
			
			
			$matchToSave = $this->MatchEntity->newEntity();
			if(isset($result[0]))
			{
				$matchToSave = $this->MatchEntity->patchEntity($matchToSave, $result[0]);
				$matchToSave->score = $this->CalculateScore($match['score']);
				$this->MatchEntity->save($matchToSave);
			}
			else
			{
				$matchToSave->score = $this->CalculateScore($match['score']);
				$matchToSave->user_match_id = $match['user_match_id'];
				$matchToSave->id = $match['user_match_id'] . '-' . $this->Auth->user()['id'];
				$matchToSave->user_id = $this->Auth->user()['id'];
				$matchToSave->is_new = true;
				$this->MatchEntity->save($matchToSave);
			}
		}
		catch(Exception $ex)
		{
			var_dump($ex);
		}
	}
	
	private function CalculateScore($score)
	{
		if($score < 10) return (($score * 10) + ($score/2));
		else if($score >= 10) return 99;
	}
	
	//TODO
	private function MatchByTag()
	{
		$favoriController = new FavoriteController();
		
		$allMyFavorite = $favoriController->FetchAllPreference();
		
		$tagList = array();
		
		$this->loadModel('TagEntity');
		$this->loadModel('ArtistTagEntity');
		
		//var_dump($allMyFavorite[0]['artist_id']['tag_list']);
		/* foreach($allMyFavorite as $fav)
		{
			/* var_dump(json_encode($fav['artist_id']['tag_list'])); 
			var_dump($allMyFavorite[0]['artist_id']['tag_list']);
			array_push($tagList, $fav['artist_id']['tag_list']);
		}
		 */
		foreach($tagList as $tag)
		{
			//$IsAlreadyUse = $this->ArtistTagEntity->findByTag_id($id, ['where ']);
		}//IWAS HERE
		
		return array();
		
	}
	
	
	public function TestMockMatch()
	{
		return ['0' => ['location' => 'france', 'username' => 'Marie', 'score' => 59, 'picture' => 'https://upload.wikimedia.org/wikipedia/commons/d/d5/Pedobear.png', 'sex' => 'WOMMEN', 'id' => '45', 'color' => '#9f173d'],
		'1' => ['location' => 'san fran', 'username' => 'Zaz0', 'score' => 41, 'picture' => 'https://c1.staticflickr.com/3/2604/3902343984_9a3242d995.jpg', 'sex' => 'WOMMEN', 'id' => '2', 'color' => '#723461'],
		'2' => ['location' => 'toledo', 'username' => 'Jean', 'score' => 41, 'picture' => 'https://c1.staticflickr.com/3/2604/3902343984_9a3242d995.jpg', 'sex' => 'WOMMEN', 'id' => '2', 'color' => '#2f6197'],
		'3' => ['location' => 'san fran', 'username' => 'Zaz2', 'score' => 41, 'picture' => 'https://c1.staticflickr.com/3/2604/3902343984_9a3242d995.jpg', 'sex' => 'WOMMEN', 'id' => '2', 'color' => '#2f6197'],
		'4' => ['location' => 'san fran', 'username' => 'Zaz3', 'score' => 41, 'picture' => 'https://c1.staticflickr.com/3/2604/3902343984_9a3242d995.jpg', 'sex' => 'WOMMEN', 'id' => '2', 'color' => '#bf0223'],
		 ];
	}
	
}