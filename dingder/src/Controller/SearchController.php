<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use App\BU\ApiClientManager;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Model\UserEntity;
use Model\ArtistEntity;
use Model\PreferenceEntity;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class SearchController extends AppController
{	
	public function Index()
    {
    }
	
	public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
		
		$_apiClientManager = new ApiClientManager;
		
		//BYPASS auth for the whole <c>SearchController</c> controller
		$this->Auth->allow("Index");
		
		
		if(isset($this->request->getParam('pass')[0]))
		{
		
			$result = $_apiClientManager->Search($this->request->getParam('pass')[0]);
			
			$this->set('result', $result);
				
			//$this->Flash->success(sizeof($result['artists']) . ' result(s) found');
				
			$this->set('query', $this->request->getParam('pass')[0]);
		}
    }
	
	public function isAuthorized($user) {
        //auth check
        //return boolean
		return true;
    }
	

    public function view($id)
    {
		parent::view($id);
    }
	
	
	
	
}