<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use App\BU\ApiClientManager;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Model\UserEntity;
use Model\ArtistEntity;
use Model\PreferenceEntity;
use Cake\Datasource\ConnectionManager;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class FavoriteController extends AppController
{	
	public function Index()
    {
		$this->redirect('/favorite/♥');
    }
	
	// made it static still not static ... wtf
	static function CompareTagCount($a, $b) 
	{
		if ($a['tag_count'] == $b['tag_count']) 
		{
			return 0;
		} 
		return ($a['tag_count'] < $b['tag_count']) ? -1 : 1;
	}

	public function beforeFilter(Event $event)
    {
		
        parent::beforeFilter($event);
		
		$this->Auth->config('authorize', false);
		
		
    }
	public function ♥()
	{
		try
		{
			$toReturn = $this->FetchAllPreference();
			//$this->Flash->success("succesfully retrieved " . count($artistList) . " artist(s)"); 
			$this->set('artistList', $toReturn);
					
		}
		catch(Cake\Datasource\Exception\RecordNotFoundException $ex)
		{
			
		}		
	}
	
	public function FetchAllPreference()
	{
		try
		{
			$this->loadModel('PreferenceEntity');
			
			$this->loadModel('ArtistTagEntity');
			
			$resultSet = $this->PreferenceEntity->find('all')->contain('ArtistEntity', 'UserEntity')->where('user_id = ' . ($this->Auth->user()['id']))->hydrate(false);
			
			$artistList = $resultSet->toArray();
			
			$toReturn = array();
			
			foreach($artistList as $artist)
			{
				//var_dump($artist);
				$tagResultSet = $this->ArtistTagEntity->find('all')->contain('TagEntity', 'ArtistEntity')->where('artist_id = \'' . $artist['artist_id']['id'] .'\'')->hydrate(false) ;
				//print_r($tagResultSet->toArray());
				
				$result = $tagResultSet->toArray();
				
				usort($result,  array($this,'CompareTagCount'));
				
				$artist['artist_id']['tag_list'] = $result;
				
				array_push($toReturn, $artist);
			}
			
			return $toReturn;
					
		}
		catch(Cake\Datasource\Exception\RecordNotFoundException $ex)
		{
			
		}		
	}
	
	public function Add()
	{
		
		$_apiClientManager = new ApiClientManager;
		
		if(isset($this->request->getParam('pass')[1]))
		{
			$artist = $_apiClientManager->Get($this->request->getParam('pass')[1]);
			
			if($artist != null && $this->SavePreference($artist))
			    $this->Flash->success('Artist saved '. $artist["name"]);
			
			else
				$this->Flash->error('Sorry an error occured'); // do not by pass login
		}
		if($this->request->getParam('pass')[0] == 'gohome')
			return $this->redirect('/');
		
		else 
			return $this->redirect('/search/' . $this->request->getParam('pass')[0]);
	}
	
	public function isAuthorized($user) 
	{
        //auth check
        //return boolean
		return true;
    }
	

    public function view($id)
    {
		parent::view($id);
    }
	
	public function SavePreference($artist)
	{
		try
		{
				
			$this->loadModel('ArtistEntity');
			$this->loadModel('PreferenceEntity');
			$this->loadModel('TagEntity');
			$this->loadModel('ArtistTagEntity');
			
			$artistToSave = $this->ArtistEntity->newEntity();
			
			$artistToSave->id = $artist['id'];
			$artistToSave->mb_name = $artist['name'];
			$artistToSave->location = $artist['area']['name'];
			$artistToSave->picture = $artist['picture'];
			if(isset($artist['youtube']))
				$artistToSave->youtube = $artist['youtube'];
			else
				$artistToSave->youtube = 'https://www.youtube.com/results?search_query=' . $artist['name'];
			
			
			if($this->ArtistEntity->save($artistToSave))
			{
			
				foreach($artist['tags'] as $tag)
				{
					//print_r($tag['count']);
					
					if($tag['count'] > 0)
					{
						$tagToSave = $this->TagEntity->newEntity();
						$artistTagToSave = $this->ArtistTagEntity->newEntity();
						
						$tagToSave->mb_name = $tag['name'];
						
						try
						{
							$this->TagEntity->save($tagToSave);
							$artistTagToSave->tag_id = $tagToSave->id;
						}
						catch(\PDOException $e)
						{
							$artistTagToSave->tag_id = $this->TagEntity->findByMb_name($tag['name'])->first()->id;
						}
						
						$artistTagToSave->id = $artistTagToSave->tag_id . $artist['id'];
						$artistTagToSave->artist_id = $artist['id'];
						
						$artistTagToSave->tag_count = (int)($tag['count']);
						
						$this->ArtistTagEntity->save($artistTagToSave);
					}
				}
				
				
				$preference = $this->PreferenceEntity->newEntity();
				
				$preference->id = $this->Auth->user()['id'] . $artist['id'];
				$preference->user_id = $this->Auth->user()['id'];
				$preference->artist_id = $artist['id'];
				$preference->group_id = null;
				$preference->added_time = date('Y-m-d H:i:s');
				
				
				
				$this->PreferenceEntity->save($preference);
				return true;
			}
			else
				return false;
		}
		catch (\PDOException $e) 
		{
			if ($e->errorInfo[1] == 1062) 
			{
				var_dump($e);
				// duplicate entry, do something else
				$this->Flash->error(__('Artist ' . $artist->name . ' already exist!'));
			} 
			else 
			{
				$this->Flash->error($e->getMessage());
			}
			return false;
        }
	}
	
	public function forget()
	{
		try
		{
			$this->loadModel('PreferenceEntity');
			
			$prefToDelete = $this->PreferenceEntity->get($this->Auth->user()['id'] . $this->request->getParam('pass')[0]);
			
			$this->PreferenceEntity->delete($prefToDelete);
			
			$this->Flash->success('The favorite has been deleted');
			
			
		}
		catch(Cake\Datasource\Exception\RecordNotFoundException $ex)
		{
			
			$this->Flash->erro('I\'m sorry an error has occured');
			
		}
		
		return $this->redirect('/favorite');
	}
	
	private function GetFromId($id)
	{
		$this->loadModel('ArtistEntity');
		
		$this->loadModel('TagEntity');
		$this->loadModel('ArtistTagEntity');
			
		$resultSet = $this->ArtistEntity->get($id);
			
		$artist = $resultSet->toArray();
				
		$tagResultSet = $this->ArtistTagEntity->find('all')->contain('TagEntity', 'ArtistEntity')->where('artist_id = \'' . $id .'\'')->hydrate(false) ;
		//print_r($tagResultSet->toArray());
				
		$result = $tagResultSet->toArray();
				
		usort($result,  array($this,'CompareTagCount'));
		//var_dump($result);
		
		$artist['artist_id']['tag_list'] = $result;
				
		return $artist;
	}
	
	public function GetMostStarred()
	{
		$toReturn = array();
		
		$conn = ConnectionManager::get('default');
				
		$stmt = $conn->execute("select *, count(artist_id) from PreferenceEntity group by artist_id order by count(artist_id) DESC limit 3"); 
		
		$result = $stmt->fetchAll('assoc');
		
						
		foreach($result as $artist)
		{
			if($artist['artist_id'])
				array_push($toReturn, $this->GetFromId($artist['artist_id']));
		}
		return $toReturn;
	}
	
	public function GetLastLiked()
	{
		$toReturn = array();
		
		$conn = ConnectionManager::get('default');
				
		$stmt = $conn->execute("Select artist_id from PreferenceEntity order by added_time desc limit 3"); 
		
		$result = $stmt->fetchAll('assoc');
		
		foreach($result as $artist)
		{
			if($artist['artist_id'])
				array_push($toReturn, $this->GetFromId($artist['artist_id']));
		}
		return $toReturn;
	}
}