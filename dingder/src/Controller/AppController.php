<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
	const HomeActivLink = ["search", ""];
	const ProfileActivLink = ["user"];
	const MatchActivLink = ["match"];
	const AboutActivLink = ["about"];
	const FavoriteActivLink = ["favorite"];
	
	public function forceSSL() 
	{
		return $this->redirect('https://' . env('SERVER_NAME') . $this->request->here);
	}

	public function beforeFilter(Event $event) 
	{		
		parent::beforeFilter($event);
		$this->Security->requireSecure();
		$this->Auth->allow(['/', 'view', 'display', '/user/login', '/user/add']);
		//$this->Auth->allow(); // ALLOW ALL ROUTE TO BE ACCESS
	}
	
	public function beforeRender(Event $event)
	{
		$this->loadComponent('Auth');
		$user = $this->Auth->user();
		if($user) $user['age'] = $this->GetAge($user['birthdate']);
		
		$this->set('CurrentUser', $user);
		
		$MatchController = new MatchController();
		
		$this->set('MatchCount', $MatchController->GetNewMatchCount());
		/* print_r($this->GetActiveLink()); */
		$this->set('ActiveLink', $this->GetActiveLink());
		
		$FavoriteController = new FavoriteController();
		$this->set('lastLiked', $FavoriteController->GetLastLiked());
		$this->set('mostStarred', $FavoriteController->GetMostStarred());
		
	}
	 
    public function initialize()
    {
        parent::initialize();
		$this->loadComponent('Security', ['blackHoleCallback' => 'forceSSL']); // SSL SECURITY
        $this->loadComponent('RequestHandler');
		$this->loadComponent('Flash');
        $this->loadComponent('Auth', [
			'authError' => 'Mind your own business or log in below',
			'authenticate' => [
				'Form' => [
				'userModel' => 'UserEntity',
				'fields' => ['email' => 'email', //Here the username is for Auth and it takes the value from field email.
				'password' => 'password']
				// 'finder' => 'auth' //Here to personalize query when login in user
				],
			],
			'authorize' => 'Controller',
			//'storage' => 'Memory',
			
            /* 'loginRedirect' => [
                'controller' => 'user',
                'action' => 'profile'
            ], */
            'logoutRedirect' => [
                'controller' => null,
                'action' => '/'
            ],
			'loginAction' => [
            'controller' => 'user',
            'action' => 'login'
			
			]
        ]);
		
        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        $this->loadComponent('Csrf');
    }
	
	public function GetAge($birthdate)
	{
		return round((time()-strtotime($birthdate))/(3600*24*365.25));
	}
	
	//DEFINES ACTIVE MENU LINK in respect of const rules
	private function GetActiveLink()
	{
		$currentController = substr($this->request->url, 0, strpos($this->request->url, "/"));
		
		//print_r($currentController);
		
		if( in_array($currentController, AppController::HomeActivLink) )
			return "home";
		
		if( in_array($currentController, AppController::ProfileActivLink) )
			return "profile";
		
		if( in_array($currentController, AppController::FavoriteActivLink) )
			return "favorite";
		
		if( in_array($currentController, AppController::MatchActivLink) )
			return "match";
		
		if( in_array($currentController, AppController::AboutActivLink) )
			return "about";
		
		else 
			return "home";
	}
}
