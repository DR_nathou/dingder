<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Http\ServerRequest;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Model\UserEntity;
use Model\ArtistEntity;
use Model\PreferenceEntity;
use Cake\Event\Event;
use Cake\Validation\Validation;
use Cake\Auth\DefaultPasswordHasher;

class UserController extends AppController
{
	public function Index()
    {
		//$this->set('user', $this->UserEntity->find('all'));
    }
	
	public function beforeFilter(Event $event)
    {
		$this->loadModel('UserEntity');
        $this->Auth->allow('add', 'logout');
		
        parent::beforeFilter($event);
    }


    public function view($id)
    {
        $user = $this->UserEntity->get($id);
        $this->set(compact('user'));
    }
	
	public function Login()
    {
		try
		{
			$this->loadModel('UserEntity');
			$matchController = new MatchController();
			if ($this->request->is('post')) 
			{	
				//var_dump($this->request->data);
				
				$user = $this->Auth->identify($this->request->data);
				
				if ($user) 
				{
					$result = $this->UserEntity->get($user['id']);
					
					$userToUpdate = $result;
					
					$userToUpdate->last_connection_date = date('Y-m-d H:i:s');
					
					$this->UserEntity->save($userToUpdate);
										
					$this->Auth->setUser($user);
					
					$this->Flash->success('W E L C O M E  ' . $user['nickname']);
					
					var_dump($matchController->MatchMe());
			
					return $this->redirect($this->Auth->redirectUrl());//$this->Auth->redirectUrl(); //
				}
				$this->Flash->error(__('Invalid username or password, try again'));
			}
		}
		catch (Exception $e) 
		{
			$this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    public function Logout()
    {
        $this->Flash->success('You are now disconected!!');
		return $this->redirect($this->Auth->logout());
    }
	
	private function GetUser()
	{
		$users= TableRegistry::get('UserEntity');
		return $users->get($this->Auth->user()['id']);
	}
	
	private function SaveUser($user)
	{
		$users= TableRegistry::get('UserEntity');
		return $users->save($user);
	}
	
	public function UpdateEmail()
	{
		if ($this->request->is('post')) 
		{
			$user = $this->GetUser();
			$newMail = strtolower($this->request->data['email']);
			$user->username = $newMail;
			$user->email = $newMail;
		
			if($this->SaveUser($user))
			{
				$this->Flash->success('Email updated!!');
			}
		}
		
		return $this->redirect('/user/profile/');
	}
	
	public function UpdateLocation()
	{
		if ($this->request->is('post')) 
		{
			$user = $this->GetUser();
			$user->location = $this->request->data['location'];
		
			if($this->SaveUser($user))
			{
				$this->Flash->success('Location updated!!');
			}
		}
		
		return $this->redirect('/user/profile/');
	}
	
	public function UpdateSex()
	{
		if ($this->request->is('post')) 
		{
			$user = $this->GetUser();
			$user->sex = $this->request->data['sex'];
			$user->picture = $this->setDefaultPicture($user->sex);
		
			if($this->SaveUser($user))
			{
				$this->Flash->success('Sex updated!!');
			}
		}		
		
		return $this->redirect('/user/profile/');
	}
	
	public function UpdateNickname()
	{
		if ($this->request->is('post')) 
		{
			$user = $this->GetUser();
			$user->nickname = $this->request->data['nickname'];
		
			if($this->SaveUser($user))
			{
				$this->Flash->success('Nickname updated!!');
			}
		}		
		
		return $this->redirect('/user/profile/');
	}
	
    public function Add()
    {
		try
		{
			$this->loadModel('UserEntity');
			$user = $this->UserEntity->newEntity();
			
			if ($this->request->is('post')) 
			{
				//var_dump($this->request->data['sex']);
				if($this->request->data['retypepassword'] == $this->request->data['password'])	
				// Prior to 3.4.0 $this->request->data() was used.
				{
					$this->request->data['email'] = strtolower($this->request->data['email'] );
					$user = $this->UserEntity->patchEntity($user, $this->request->data, ['validate' => 'hardened']);
					$user->registration_date = date('Y-m-d H:i:s');
					$user->last_connection_date = date('Y-m-d H:i:s');
					$user->username = $user->email;
					$user->picture = $this->setDefaultPicture($user->sex);
					
					
					if ($this->UserEntity->save($user)) 
					{
						//$this->Flash->success(__('The user has been saved.'));
						
						$this->Auth->setUser($user);
						
						$this->Flash->success('W E L C O M E  ' . $user['nickname']);
												
						return $this->redirect('/user/profile');
						
					}
					else
					{
						$this->Flash->error(__('Sorry can\'t save you mate: ' . count($user['errors'])));
				
					}
				}
				else 
				{
					$this->Flash->error(__('Passwords don\'t match'));
				}
			}
			
		} 
		catch (\PDOException $e) 
		{
			if ($e->errorInfo[1] == 1062) 
			{
				//var_dump($e);
				// duplicate entry, do something else
				$this->Flash->error(__('user ' . $user->nickname . ' already exist!'));
			} 
			else 
			{
				var_dump($e);
				// an error other than duplicate entry occurred
				$this->Flash->error(__('Unable to add the user.'));
			}
			
        }
        
    }
	
	private function setDefaultPicture($sex)
	{
		if($sex == 'Female') return "/img/defaultprofilegirl.png";
		else if($sex == 'Male') return "/img/defaultprofileboy.png";
		else if ($sex == 'undetermined') return "/img/defaultprofileundetermined.png";
		else return "/img/defaultprofileundetermined.png";
	}
	
	public function isAuthorized($user) 
	{
        //auth check
        //return boolean
		return true;
    }
	
	public function Profile()
	{
		$users= TableRegistry::get('UserEntity');
		$user = $users->get($this->Auth->user()['id']);
		$user = $user->toArray();
		if($user) $user['age'] = $this->GetAge($user['birthdate']);
		
		$this->set('me', $user);
	}
	
	
	
	private function HashMe($password)
	{
		
	
	}
}
