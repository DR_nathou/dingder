
	<footer id="footer" class="top-space">

		<div class="footer1">
			<div class="container">
				<div class="row">
					
					<div class="col-md-3 widget">
						<h3 class="widget-title">Contact <i class="fas fa-terminal"></i></h3>
					</div>
					

					<div class="col-md-3 widget">
						<div class="widget-body">
							<p>
								<a href="mailto:nathan.owski@gmail.com?subject=I ❤️ Your WebSite🐌&body=Hi 🖖 Tell me more..." style="font-size:medium"> <i class="far fa-envelope fa-3x"></i> </a>
								
							</p>	
						</div>
					</div>
					
					<div class="col-md-3 widget">
						<h3 class="widget-title">Follow me <i class="fas fa-terminal"></i></h3>
					</div>
					

					<div class="col-md-3 widget">
						<div class="widget-body">
							<p>
								<a href="https://nathanowski.alwaysdata.net/"><i class="far fa-hand-pointer fa-5x"></i></a>
								<a href="https://bitbucket.org/DR_nathou/dingder"><i class="fab fa-bitbucket fa-5x"></i></a>
								<a href="mailto:nathan.owski@gmail.com?subject=I ❤️ Your WebSite🐌&body=Hi 🖖 Tell me more..."><i class="fas fa-microchip fa-5x"></i></a>
								
								<a href="https://stackoverflow.com/users/5908101/nathan-smiechowski?tab=profile"><i class="fab fa-stack-overflow fa-5x"></i></a>
								
							</p>	
						</div>
					</div>

					

				</div> <!-- /row of widgets -->
			</div>
		</div>

		<div class="footer2">
			<div class="container">
				<div class="row">
					
					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="simplenav">
								<a href="#">Home</a> | 
								<a href="#">About</a> |
								<a href="mailto:nathan.owski@gmail.com?subject=I ❤️ Your WebSite🐌&body=Hi 🖖 Tell me more...">Contact</a> |
								<b><a href="user/add">Sign up</a></b>
							</p>
						</div>
					</div>

					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="text-right">
								Copyright &copy; <?= date('Y');?>, Designed by <a href="http://gettemplate.com/" rel="designer">gettemplate</a> and DR_nathou 
							</p>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>
	</footer>	
		




	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="/js/headroom.min.js"></script>
	<script src="/js/jQuery.headroom.min.js"></script>
	<script src="/js/template.js"></script>
</body>
</html>