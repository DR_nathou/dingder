<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="DRznathou template by Sergey Pozhilov (GetTemplate.com)">
	<link rel="apple-touch-icon" sizes="57x57" href="/img/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/img/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/img/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/img/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/img/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/img/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/img/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/img/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/img/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/img/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/img/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
	<link rel="manifest" href="/img/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/img/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<title>Dingder Listen n Meet</title>

	<link rel="shortcut icon" href="favicon.ico">
	
	<link rel="stylesheet" media="screen" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="/css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries
	[if lt IE 9]> -->
	<script src="/js/html5shiv.js"></script>
	<script src="/js/respond.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	<style>
	.dot {
		height: 25px;
		width: 25px;
		background-color: #D33C44;
		border-radius: 50%;
		position:absolute;
		display: inline-block;
		font-size: medium;
		text-align:center;
		color: white;
		z-index: 10;
	}
	</style>
	<!-- WARNING JAVASCRIPT JQUERY NOOB WARNING--> 
	<script>
		$(document).on("keypress", "input", function(e){
			if(e.which == 13){
			
				Search();
				//var query = $(this).val().replace("➜ ", "");
				//if(query.length >= 3)
				//	window.location.replace("/search/" + query);
			}
		});
		function Search()
		{
			var query = (document.getElementById("query").value).replace("➜ ", ""); // doesnt work ... .replace("/", " ")
			// debug purpose alert(query);
			if(query.length >= 3)
				window.location.replace("/search/" + query);
		}
	
	</script>
	<!-- WARNING JAVASCRIPT JQUERY NOOB WARNING--> 
	
	
	<!-- [endif]--> 
</head>

 <body class="home">
	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top headroom" >
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->	
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="navbar-brand" href="/"><img src="/img/logo.png" alt="Dingder"></a>
				
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li >
					<!-- onkeyup="Search()" -->
					<input class="dosearch" id="query" type="search" border-radius="10px" value="<?php if (isset($query)) echo '➜ ' . $query; else echo '➜ '?>" text-transform="uppercase"/>
						

					</li>
					<li><button class="dosearch" onclick="Search()" ><i class="fas fa-search fa-1x"></i></button></li>
					<li <?php if($ActiveLink == 'home') echo 'class="active"' ?>><a href="/"><i class="fab fa-rebel"></i> Home</a></li>
					<li <?php if($ActiveLink == 'profile') echo 'class="active"' ?>><a href="/user/profile"><i class="far fa-user"></i> Profile</a></li>
					<li <?php if($ActiveLink == 'favorite') echo 'class="active"' ?>><a href="/favorite/♥"><i class="far fa-thumbs-up"></i> Favorites</a></li>
					<!-- <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">More Pages <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="sidebar-left.html">Left Sidebar</a></li>
							<li class="active"><a href="sidebar-right.html">Right Sidebar</a></li>
						</ul>
					</li> -->
					<li <?php if($ActiveLink == 'match') echo 'class="active"' ?> style="position:relative"><span class="dot" style="visibility:<?php if($MatchCount == 0) echo 'hidden'; else echo 'visible'?>"><?= $MatchCount ?></span><a href="/match"><i class="far fa-heart"></i> Match Me</a></li>
					
					<!-- <li <?php if($ActiveLink == 'about') echo 'class="active"' ?>><a href="#"><i class="far fa-hand-rock"></i> About</a></li> -->
					
					<li><?php 
						if(!$CurrentUser)
						{ echo '<a class="btn" href="/user/login">SIGN IN <i class="fas fa-plug"></i> SIGN UP</a>';} 
						else 
						{ echo '<a class="btn" href="/user/logout">SIGN OFF <i class="fas fa-plug"></i> </a>';}  ?>
					</li>
				</ul>
			</div><!--/.nav-collapse -->
			
		</div>
			<?= $this->Flash->render() ?>
	</div>
	
	<!-- /.navbar -->
	