<?= $this->element('header'); ?>
	
<!-- <body styles="background-image: url('https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Music-beam.svg/2000px-Music-beam.svg.png'); background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center;">
</body> -->
	<!-- Header -->
	<!-- <header id="head"> -->
		<!-- <div class="container"> -->
			<!-- <div class="row"> -->
				<!-- <h1 class="lead">AWESOME, CUSTOMIZABLE, FREE</h1> -->
				<!-- <img source="img/1.jpg"></img> -->
				<!-- <p class="tagline">PROGRESSUS: free business bootstrap template by <a href="http://www.gettemplate.com/?utm_source=progressus&amp;utm_medium=template&amp;utm_campaign=progressus">GetTemplate</a></p> -->
				<!-- <p><a class="btn btn-default btn-lg" role="button">MORE INFO</a> <a class="btn btn-action btn-lg" role="button">DOWNLOAD NOW</a></p> -->
			<!-- </div> -->
		<!-- </div> -->
	<!-- </header> -->
	<!-- /Header -->
	
<div class="container">
	<div class="row">
		<article class="col-xs-12 maincontent">
			<header class="page-header">
				<h4 class="page-title"><?php
				if(isset($result) && isset($query))
				{
					echo 'Search returns ' . count($result['artists']) .' result(s) querying: <b>' . $query . '</b>';
				}
				else echo 'Search returns literally nothing, void, nada';
				?></h4>
			</header>
				
			<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				<div class="panel panel-default">
				<!-- style="overflow:hidden;" -->
					<div class="panel-body">
					 <!-- style="overflow:auto; height:350px;" -->
						
						<hr>

	
							<div class="row">
								<?php
									if(isset($result))
									{
										$i =0;
										$lastElement = end($result['artists']);
										foreach($result['artists'] as $artist)
										{
											if($i == 3)
											{
												echo '</div>
														<!-- <div class="row" style="width:inherit;">
															<div style="width:inherit;padding-top:5px;text-align:center;border-radius:10px;background-color:red;">
																<a href="#'.$artist['id'].'"><i class="far fa-caret-square-down 3x"></i></a>
															</div>
														</div> -->
													<div class="row">';
												$i = 0;
											}
											$i++;
											echo '<div id="'.$artist['id'].'" class="col-sm-4"><div class="card" style="width: 18rem;">
											  <img class="card-img-top" src="/img/record.png" alt="Card image cap">
												  <div class="card-body">
													<h5 class="card-title">'.$artist['name'].'</h5>
													<a href="/favorite/add/'.$query.'/'.$artist['id'].'" class="btn btn-primary" style="border-radius:10px;">-- I <i class="far fa-heart"></i> this --</a>
												  </div>
											  </div>
											</div>';
											if($artist == $lastElement) echo '</div>';
										}
									}
								?>
						<hr>

					</div>
				</div>

			</div>
			
		</article>
		<!-- /Article -->

	</div>
</div>	<!-- /container -->

<?= $this->element('footer'); ?>