<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;

$this->layout = 'error';

if (Configure::read('debug')) :
    $this->layout = 'dev_error';

    $this->assign('title', $message);
    $this->assign('templateName', 'error400.ctp');

    $this->start('file');
?>
<?php if (!empty($error->queryString)) : ?>
    <p class="notice">
        <strong>SQL Query: </strong>
        <?= h($error->queryString) ?>
    </p>
<?php endif; ?>
<?php if (!empty($error->params)) : ?>
        <strong>SQL Query Params: </strong>
        <?php Debugger::dump($error->params) ?>
<?php endif; ?>
<?= $this->element('auto_table_warning') ?>
<?php
if (extension_loaded('xdebug')) :
    xdebug_print_function_stack();
endif;

$this->end();
endif;
?>
<h2><?= h($message) ?></h2>
<p class="error">
    <strong><?= __d('cake', 'Error') ?>: </strong>
    <?= __d('cake', 'The requested address {0} was not found on this server.', "<strong>'{$url}'</strong>") ?>
</p>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="http://schema.org/WebPage">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, minimal-ui" />
    <meta name="viewport" id="vp" content="initial-scale=1.0, user-scalable=no, maximum-scale=1.0, minimal-ui" media="(device-height: 568px)" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="cleartype" content="on">

    <title>404 | nclud - A Provocative Creative Agency</title>
    <meta name="description" content="The page you requested cannot be found right meow.">
    <link rel="canonical" href="http://nclud.com/404/">
    <meta name="google-site-verification" content="YIoP3X4vrxtonzGBlcowIfZ_RDkjR4M8Oe8gG_1ruXA" />

    <link rel="author" href="humans.txt" />
    <meta name="robots" content="noydir,noodp">
    <meta name="theme-color" content="#ac2eff">

    <link rel="apple-touch-icon-precomposed" sizes="180x180" href="/apple-180.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/apple-152.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-144.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/apple-120.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/apple-114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/apple-72.png">
    <link rel="apple-touch-icon-precomposed" href="/android-152.png">
    <link rel="mask-icon" href="/pin-icon.svg" color="#ac2eff">

    <link rel="icon" href="/fav16.png" sizes="16x16" type="image/png">
    <link rel="icon" href="/fav32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/fav48.png" sizes="48x48" type="image/png">
    <link rel="icon" href="/fav64.png" sizes="64x64" type="image/png">
    <link rel="icon" href="/fav128.png" sizes="128x128" type="image/png">
    <link rel="icon" href="/fav195.png" sizes="195x195" type="image/png">
    <link rel="shortcut icon" href="/fav196.png" sizes="196x196">

    <link rel="icon" href="/fav32.png">
    <!--[if lte IE 8]><link rel="shortcut icon" href="favicon.ico"><![endif]-->

    <meta name="msapplication-TileColor" content="#ac2eff">
    <meta name="msapplication-TileImage" content="/fav144-ie10.png">
	<meta name="msapplication-square70x70logo" content="/fav128-ie11.png"/>
	<meta name="msapplication-square150x150logo" content="/fav270-ie11.png"/>
	<meta name="msapplication-wide310x150logo" content="/fav558x270-ie11.png"/>
	<meta name="msapplication-square310x310logo" content="/fav558-ie11.png"/>

    <script type="application/ld+json">
	    {
			"@context": "http://schema.org",
			"@type": "Organization",
			"name": "nclud",
			"url": "https://nclud.com",
			"logo": "https://nclud.com/img/nclud-logo.png",
			"contactPoint" : [{
				"@type": "ContactPoint",
				"telephone": "+1-202-684-8984",
				"contactType": "customer service",
				"areaServed": "US"
			}],
			"sameAs": [
				"https://www.facebook.com/nclud",
    			"https://twitter.com/nclud",
    			"https://www.linkedin.com/company/nclud"
    		]
	    }
    </script>
	<script type="application/ld+json">
	    {
			"@context" : "http://schema.org",
			"@type" : "WebSite",
			"name" : "nclud",
			"alternateName" : "nclud - A Provocative Creative Agency",
			"url" : "https://nclud.com"
	    }
    </script>
    <script type="application/ld+json">
    	{
    		"@context": "http://schema.org",
			"@type": "PostalAddress",
			"streetAddress": "1424 K St NW, 3rd Floor",
			"addressLocality": "Washington",
			"addressRegion": "DC",
			"postalCode": "20005"
    	}
    </script>

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@nclud">
    <meta name="twitter:title" content="404 | nclud - A Provocative Creative Agency">
    <meta name="twitter:description" content="The page you requested cannot be found right meow.">
    <meta name="twitter:image:src" content="http://nclud.com/social.jpg">

    <meta itemprop="name" content="404 | nclud - A Provocative Creative Agency" />
    <meta itemprop="description" content="The page you requested cannot be found right meow." />
    <meta itemprop="image" content="http://nclud.com/social.jpg" />

    <meta property="og:title" content="404 | nclud - A Provocative Creative Agency" />
    <meta property="og:url" content="http://nclud.com/404/"/>
    <meta property="og:site_name" content="nclud - A Provocative Creative Agency" />
    <meta property="og:description" content="The page you requested cannot be found right meow." />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="http://nclud.com/social.jpg" />

    <link href="/css/style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.polyfill.io/v1/polyfill.min.js"></script>
    <script>window.lazySizesConfig = window.lazySizesConfig || {}; lazySizesConfig.lazyClass = 'lazyload'; lazySizesConfig.srcAttr = 'data-original'; lazySizesConfig.loadMode = 2; lazySizesConfig.expand = 600; lazySizesConfig.expFactor = 3;</script>
    
	<script src="/js/lib.js"></script>
	<script src="/js/three.js"></script>
</head>

<body class="initial-load">

<div id="main-body">
<section id="page-structure" class="ajax four-oh-four">


<a href="/" class="home-link">Home></a>

<ul id="site-nav">
    <li><a href="/work/" class="work-link"></a></li>
    <li><a href="/about/" class="about-link hidden"></a></li>
    <li><a href="/contact/" class="contact-link"></a></li>
</ul>

<form class="four-oh-four-form">
    <input type="text" class="404-input">
</form>

<div class="terminal padded">
    <p class="prompt">The page you requested cannot be found right meow. Try home, about, work, or contact.</p>
    <p class="prompt output new-output"></p>
</div>

<script>window.viewportUnitsBuggyfill.init();</script>

</section>
</div>

<script type="text/javascript">
	var _mfq = _mfq || [];
	(function() {
		var mf = document.createElement("script"); mf.type = "text/javascript"; mf.async = true;
		mf.src = "//cdn.mouseflow.com/projects/699c07ab-d2e1-471d-a93c-b421434791f6.js";
		document.getElementsByTagName("head")[0].appendChild(mf);
	})();
	var mouseflowPath = document.location.pathname;
	if ($(window).width() <= 480) mouseflowPath += "/smartphone";
	else if ($(window).width() <= 840) mouseflowPath += "/tablet";
</script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-1267683-1', 'auto');
	ga('send', 'pageview');
</script>
<noscript><iframe src="//www.googletagmanager.com/ns/?id=GTM-5S58PJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5S58PJ');</script>
<script type="text/javascript" src="/js/footer.js"></script>
</body>
</html>

