<?= $this->element('header'); ?>
	

	<!-- container -->
	<div class="container" >

		

		<div class="row">
			<!-- Article main content -->
			<article class="col-xs-12 maincontent" style="margin-bottom: 150px;">
			
			
				
			
				<header class="page-header">
				
					<h1 class="page-title"> <?php 	if(isset($artistList) && count($artistList) > 0)
														{ echo ' <i class="far fa-star 5x"></i> ' . count($artistList) . ' favorites' ; } 
													else echo ' <i class="far fa-star 5x"></i> 0 favorite <a href="/search/pink floyd"> <i class="fas fa-forward"></i> </a>'; ?> </h1>
				</header>
				
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				
					<div class="panel panel-default" >
					<!-- style="overflow:auto;" -->
						<div class="panel-body" >
						<!-- style="overflow:auto; height:350px;" -->
						
							<div class="row">						
								<?php
									if(isset($artistList))
									{
										$i =0;
										$lastElement = end($artistList);
										
										
										foreach($artistList as $artist)
										{
											
											//print_r(json_encode($artist, JSON_PRETTY_PRINT));
											$artist = $artist['artist_id'];
											
											$img = '/img/record.png';
											
											if(isset($artist['picture']) && $artist['picture'] != 'www' && $artist['picture'] != '')
												$img = $artist['picture'];
												
											if($i == 3)
											{
												echo '</div>
														<!-- <div class="row" style="width:inherit;">
															<div style="width:inherit;padding-top:5px;text-align:center;border-radius:10px;background-color:red;">
																<a href="#'.$artist['id'].'"><i class="far fa-caret-square-down 3x"></i></a>
															</div>
														</div> -->
													<div class="row">';
												$i = 0;
											}
											$i++;
											
											$tagList = array();
											foreach($artist['tag_list'] as $tag)
											{
												array_push($tagList, $tag['tag_id']['mb_name']);
											}
											$firstTag = '';
											if (count($tagList) > 0) $firstTag = $tagList[0];
											
											
											echo '<div id="'.$artist['id'].'" class="col-sm-4" style="padding:10px;"><div class="card" style="width: 18rem; text-align:center">
											  <img class="card-img-top" style="height:200px;width:256px;object-fit: cover;" src="'.$img.'" alt="artist image">
												  <div class="card-body">
													<h2 class="card-title">'.$artist['mb_name'].'</h2>
													<h4 class="card-title" >' .$artist['location'].'</h4>
													<a class="card-title" href="'.$artist['youtube'].'"> Youtube link <i class="fab fa-youtube"></i></a>
													
													<div class="tagtooltip card-title">' . $firstTag . '<span class="tagtooltiptext">'. join(" ", $tagList) .'</span></div>
													
													<a href="/favorite/forget/'.$artist['id'].'" class="btn btn-primary" style="border-radius:10px; width:100%"> I <i class="far fa-thumbs-down"></i> this </a>
													
												  </div>
											  </div>
											</div>';
											if($artist == $lastElement) echo '</div>';
										}
									}
								?>
							<hr>
						</div>
					</div>

				</div>
				
			</article>
			<!-- /Article -->

		</div>
	</div>	<!-- /container -->
	<!-- 
	imagecrop(imgfromjpg $img, ['x' => 0, 'y' => 0, 'width' => 324, 'height' => 324])  -->
<?= $this->element('footer'); ?>