<?= $this->element('header'); ?>
	
	<style>
	.match-btn-left
	{
		background: #1078b1; /* Old browsers */
		background: -moz-linear-gradient(left, #1078b1 0%, #84466c 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(left, #1078b1 0%,#84466c 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to right, #1078b1 0%,#84466c 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1078b1', endColorstr='#84466c',GradientType=1 ); 
	}
	
	.match-btn-right
	{
		/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#84466c+0,f51528+100 */
		background: #84466c; /* Old browsers */
		background: -moz-linear-gradient(left, #84466c 0%, #f51528 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(left, #84466c 0%,#f51528 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to right, #84466c 0%,#f51528 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#84466c', endColorstr='#f51528',GradientType=1 ); /* IE6-9 */
	}
	
	</style>
	
	<!-- container -->
	<div class="container">

		
		<div class="row">
			<!-- Article main content -->
			<article class="col-xs-12 maincontent" style="margin-bottom: 150px;">
			
				<header class="page-header">
				
					<h1 class="page-title"> <?php 	if(isset($MatchCount) && $MatchCount > 0)
													{ 
														if($MatchCount == 1) 
															echo ' <i class="far fa-heart"></i> ' . $MatchCount . ' match <i class="far fa-meh"></i>' ; 
														else
															echo ' <i class="far fa-heart"></i> ' . $MatchCount . ' matchs </i> <i class="far fa-smile"></i>' ;
													} 
													else 
														echo ' <i class="far fa-heart"></i> 0 Match <i class="far fa-frown"></i> '; ?> </h1>
				</header>
				
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				
					<div class="panel panel-default" >
					<!-- style="overflow:auto;" -->
						<div class="panel-body" >
						<!-- style="overflow:auto; height:350px;" -->
						
							<div class="row">	
												
									
										<?php
											if(isset($MatchList))
											{
												$COLOR = [0 => '#1078B1', 20 => '#2f6197', 40 => '#4d4c7f', 60 => '#723461', 80 => '#9f173d', 100 => '#bf0223'];
												$lastElement = end($MatchList);
												$firstElement = current($MatchList);
																						
												foreach($MatchList as $match)
												{
													if($match['is_ignored'] == true) continue;
													$user = $match['user_match_id'];
													
													////usefull readable debug function
													//print_r(json_encode($match, JSON_PRETTY_PRINT));
													////usefull readable debug function
													$fontAwesomeSvg = '<i class="far fa-thumbs-up 2x"></i>';
													if($match['is_liked']) 
														$fontAwesomeSvg = '<i class="fas fa-thumbs-up 2x"></i>';
													
													$score = (int)$match['score'];
				 
													if($score > 0   && $score < 20)       { $color = $COLOR[0]  ; }
													else if($score >= 20 && $score < 40)  { $color = $COLOR[20] ; }
													else if($score >= 40 && $score < 60)  { $color = $COLOR[40] ; }
													else if($score >= 60 && $score < 80)  { $color = $COLOR[60] ; }
													else if($score >= 80 && $score < 90)  { $color = $COLOR[80] ; }
													else if($score >= 90 && $score < 100) { $color = $COLOR[100]; }
													
													echo '
														<div id="'.$match['id'].'" class="col-sm-4" style="padding:10px;">
															<div class="card" style="width: 18rem; text-align:center">
																<img class="card-img-top" style="height:200px;width:256px;object-fit: cover;" src="'.$user['picture'].'" alt="artist image">
																	<div class="card-body">
																		<h2 class="card-title">' . $user['nickname'].'</h3>
																		<h4 class="card-title" >' .$user['location'].'</h4>
																		<h4 class="card-title" >' .$user['sex'].'</h4>
																		<h4 class="card-title" style="color:'.$color.'">Matching <b>' .$match['score'].' %</b> </h4>
																		<div class="row">
																			<div class="col-xs-6" style="margin-right:-20px">
																				<a href="/match/ignore/'.$match['id'].'" class="btn btn-primary match-btn-left" style="border-radius:10px 0px 0 10px; width:50%"><i class="far fa-thumbs-down 2x"></i></a>
																			</div>
																			<div class="col-xs-6" style="margin-left:0px">
																				<a href="/match/like/'.$match['id'].'" class="btn btn-primary  match-btn-right" style="border-radius:0px 10px 10px 0px; width:50%">'.$fontAwesomeSvg.'</a>
																			</div>
																		</div>
																</div>
															</div>
														</div>'
													;
												
												}
											}
										?>
									
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</article>
			<!-- /Article -->
		</div>
	</div>	<!-- /container -->
	<!-- 
	imagecrop(imgfromjpg $img, ['x' => 0, 'y' => 0, 'width' => 324, 'height' => 324])  -->
<?= $this->element('footer'); ?>