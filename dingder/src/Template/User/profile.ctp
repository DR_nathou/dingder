<?= $this->element('header'); ?>
	
<script>
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});

function ShowHide(id) {
    var x = document.getElementById(id);
    if (x.style.display != "block") 
	{
        x.style.display = "block";
    } 
	else 
	{
        x.style.display = "none";
    }
} 

</script>


<style>

.HiddenForm
{
	display:none;
}

.avatar-upload {
  position: relative;
  max-width: 205px;
}
.avatar-upload .avatar-edit {
  position: absolute;
  right: 12px;
  z-index: 10;
  top: 10px;
}
.avatar-upload .avatar-edit input {
  display: none;
}
.avatar-upload .avatar-edit input + label {
  display: inline-block;
  width: 34px;
  height: 34px;
  margin-bottom: 0;
  border-radius: 100%;
  background: #FFFFFF;
  border: 1px solid transparent;
  box-shadow: 0px 2px 4px 0px #1078B1;
  cursor: pointer;
  font-weight: normal;
  transition: all 0.2s ease-in-out;
}
.avatar-upload .avatar-edit input + label:hover {
  background: #f1f1f1;
  border-color: #d6d6d6;
}
.avatar-upload .avatar-edit input + label:after {

  content: "✎";
  color: #757575;
  position: absolute;
  top: 10px;
  left: 0;
  right: 0;
  text-align: center;
  margin: auto;
}
.avatar-upload .avatar-preview {
  position: relative;
  border-radius: 100%;
  border: 6px solid transparent;
  box-shadow: 0px 2px 4px 0px #1078B1;
}
.avatar-upload .avatar-preview > img {
  width: 100%;
  height: 100%;
  border-radius: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
}

.profileform
{
    float: right;
	
}

</style>
	<!-- container -->
	<div class="container" style="overflow:auto;">
<!-- 
		<ol class="breadcrumb">
			<li><a href="/">Home</a></li>
			<li class="active">Profile</li>
		</ol> -->

		<div class="row">
			
			<!-- Article main content -->
			<article class="col-xs-12 maincontent">
				<header class="page-header">
					<h1 class="page-title"> <i class="far fa-user"></i> Hi <?php echo h($me['nickname'])?></h1>
				</header>
				
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				
					<div class="panel panel-default" >
					<!-- style="overflow:auto;" -->
						<div class="panel-body">
						
							
														
							<div class="text-left avatar-upload">
							
								<div class="avatar-edit">
									<input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
									<label for="imageUpload"></label>
								</div>
								
								<div class="avatar-preview">
									<img id="imagePreview" style="height:250px;width:175; object-fit: cover; background-image: url(<?= $me['picture']?>);">
										<!-- src="/img/defaultprofileboy.png"></img> -->
									</img>
								</div>
							</div>
							
							<hr>
							
							<h3 class="thin text-left" style="color:#1078B1">Email</h3>
							<h3 class="text-right text-muted"><?= $me['email']?></h3>
							<!-- <h3 class="text-right text-muted"><?= $me['email']?> <i class="fas fa-pencil-alt 6x" onclick="ShowHide('HiddenFormEmail')"></i> </h3> -->
							
							<div class="HiddenForm form" id="HiddenFormEmail">
							
								<div class="row">
									<?= $this->Form->create('UpdateEmail',['url' => '/user/updateemail']) ?>
								
									<div class="col-xs-6">
								
										<fieldset>
											<?= $this->Form->input('email', ['name' => 'email','type'=>'text', 'value'=>$me['email'], 'class' => 'profileform']) ?>
									
										</fieldset>
									</div>
									<div class="col-xs-6">
										<button type="submit" >OK</button>
										<?= $this->Form->end() ?>
									</div>
								</div>
							</div>
							<hr>
							
							<h3 class="thin text-left" style="color:#1078B1">Nickname</h3>
							<h3 class="text-right text-muted"><?= $me['nickname']?> <i class="fas fa-pencil-alt 6x" onclick="ShowHide('HiddenFormNickname')"></i> </h3>
							
							<div class="HiddenForm form" id="HiddenFormNickname">
							
							
								<div class="row">
									<?= $this->Form->create('updateNickname',['url' => '/user/updatenickname']) ?>
								
									<div class="col-xs-6">
								
										<fieldset>
											<?= $this->Form->input('', ['name' => 'nickname','type'=>'text', 'value'=>$me['nickname'], 'class' => 'profileform']) ?>
									
										</fieldset>
									</div>
									<div class="col-xs-6">
										<button type="submit" >OK</button>
										<?= $this->Form->end() ?>
									</div>
								</div>
							</div>
							<hr>
							
							<h3 class="thin text-left" style="color:#1078B1">Location</h3>
							<h3 class="text-right text-muted"><?php if(isset($me['location'])) echo $me['location']?> <i class="fas fa-pencil-alt 6x" onclick="ShowHide('HiddenFormLocation')"></i> </h3>
							
							<div class="HiddenForm form" id="HiddenFormLocation">
							
							
								<div class="row">
									<?= $this->Form->create('',['url' => '/user/updatelocation']) ?>
								
									<div class="col-xs-6">
								
										<fieldset>
											<?= $this->Form->input('', ['name' => 'location','type'=>'text', 'value'=>$me['location'], 'class' => 'profileform']) ?>
									
										</fieldset>
									</div>
									<div class="col-xs-6">
										<button type="submit" >OK</button>
										<?= $this->Form->end() ?>
									</div>
								</div>
							</div>
							<hr>
							<h3 class="thin text-left" style="color:#1078B1">Sex</h3>
							<h3 class="text-right text-muted"><?= $me['sex']?> <i class="fas fa-pencil-alt 6x" onclick="ShowHide('HiddenFormSex')"></i> </h3>
							
							<div class="HiddenForm form" id="HiddenFormSex">
							
							
								<div class="row">
									<?= $this->Form->create('',['url' => '/user/updatesex']) ?>
								
									<div class="col-xs-6">
								
										<fieldset>
											<input type="radio" id="Female" name="sex" <?php if($me['sex'] == 'Female') echo 'checked'?> value="Female">
													<label for="Female">Female</label>
												<input type="radio" id="Male" name="sex" <?php if($me['sex'] == 'Male') echo 'checked'?> value="Male">
													<label for="Male">Male</label>
												<input type="radio" id="undetermined" name="sex" <?php if($me['sex'] == 'undetermined') echo 'checked'?> value="undetermined">
													<label for="undetermined">undetermined</label>
										</fieldset>
									</div>
									<div class="col-xs-6">
										<button type="submit" >OK</button>
										<?= $this->Form->end() ?>
									</div>
								</div>
							</div>
							<hr>
							<h3 class="thin text-left" style="color:#1078B1">Age</h3>
							<h3 class="text-right text-muted"><?= $me['age']?></h3>
						</div>
						<div class="panel-body" >
						<!-- style="overflow:auto; height:350px;" -->
						
							<div class="row">						
								
						</div>
					</div>

				</div>
				
			</article>
			<!-- /Article -->

		</div>
	</div>	<!-- /container -->
	<!-- 
	imagecrop(imgfromjpg $img, ['x' => 0, 'y' => 0, 'width' => 324, 'height' => 324])  -->
<?= $this->element('footer'); ?>