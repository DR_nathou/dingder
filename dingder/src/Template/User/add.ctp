<?= $this->element('header'); ?>
	

	<!-- container -->
	<div class="container">


		<div class="row">
			
			<!-- Article main content -->
			<article class="col-xs-12 maincontent">
				<header class="page-header">
					<h1 class="page-title">Sign in</h1>
				</header>
				
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<div class="panel panel-default">
						<div class="panel-body">
							<h3 class="thin text-center">JOIN millions of music lovers</h3>
							
							<hr>
							
							
							<div class="users form">
							
								<?= $this->Flash->render() ?>
								<?= $this->Form->create('FormName') ?>
									<fieldset>
										<legend><?= __('Add User') ?></legend>
										<?= $this->Form->input('email', ['name' => 'email','type'=>'text', 'required'=>true, 'label'=>'Email']) ?>
										<?= $this->Form->input('nickname', ['name' => 'nickname', 'type'=>'text', 'required'=>true, 'label'=>'Nickname']) ?>
										<?= $this->Form->input('password', ['name' => 'password', 'type'=>'password', 'required'=>true, 'label'=>'Password']) ?>
										<!-- <?= $this->Form->input('retypepassword', ['name' => 'retypepassword', 'type'=>'password', 'required'=>true, 'label'=>'Please write your password one more time'], 'onfocusout="BothFieldsIdenticalCaseSensitive()"') ?> -->
										
										<div class="input password required">
											<label for="retypepassword">Please write your password one more time</label>
											<input name="retypepassword" required="required" id="retypepassword" onfocusout="BothFieldsIdenticalCaseSensitive()" type="password">
										</div>
										<div class="row">
											<div class="col-sm-6">
												<?= $this->Form->control('birthdate', ['label' => 'Date of birth', 'type'=>'date', 'dateFormat'=> 'DMY', 'minYear' => date('Y') - 70, 'maxYear' => date('Y') - 18]); ?>
											</div>
											<div class="col-sm-6">
												<input type="radio" id="Female" name="sex" value="Female">
													<label for="Female">Female</label>
												<input type="radio" id="Male" name="sex" value="Male">
													<label for="Male">Male</label>
												<input type="radio" id="undetermined" name="sex" checked value="undetermined">
													<label for="undetermined">undetermined</label>
												<!-- <?= $this->Form->radio('sex', ['Female', 'Male', 'Undetermined'], ['required'=>false, 'label'=>'Sex']) ?> -->
											</div>
										</div>
										
										<?= $this->Form->input('location', ['name' => 'location', 'type'=>'text', 'required'=>false, 'label'=>'location']) ?>
										
										
								   </fieldset>
								<?= $this->Form->button(__('Submit')); ?>
								<?= $this->Form->end() ?>
								</div>

								<hr>

								<!-- <div class="row">
									<div class="col-lg-8">
										<b><a href="">Forgot password?</a></b>
									</div>
									<div class="col-lg-4 text-right">
										<button class="btn btn-action" type="submit">Sign in</button>
									</div>
								</div> -->
							</form>
						</div>
					</div>

				</div>
				
			</article>
			<!-- /Article -->

		</div>
	</div>	<!-- /container -->
	<script type="text/javascript" language="JavaScript">

function BothFieldsIdenticalCaseSensitive() 
{

	var one = password.value;
	var another = retypepassword.value;

	if(one == another) { retypepassword.style.borderColor = "#CCCCCC" ;return true; }
	retypepassword.style.borderColor = "red"; 
	return false;
}

</script>


	
<?= $this->element('footer'); ?>