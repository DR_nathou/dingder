<?= $this->element('header'); ?>
	

	<!-- container -->
	<div class="container">

		

		<div class="row">
			
			<!-- Article main content -->
			<article class="col-xs-12 maincontent">
				<header class="page-header">
					<h1 class="page-title">Sign in</h1>
				</header>
				
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<div class="panel panel-default">
						<div class="panel-body">
							<h3 class="thin text-center">You can also <a href="add" style="font-size:xx-large;color:#D33C44">Sign up here <i class="far fa-hand-rock"></i></a>  and join millions of music lovers </h3>
							<hr>
							
							<div class="users form">
								
									 <?= $this->Form->create() ?>
									<fieldset>
										<!-- <legend><?= __('Login') ?></legend> -->
										<?= $this->Form->input('username', ['name' => 'username','type'=>'text', 'required'=>true, 'label'=>'E-mail']) ?>
										<?= $this->Form->input('password', ['name' => 'password', 'type'=>'password', 'required'=>true, 'label'=>'Password']) ?>
								   </fieldset>
								<?= $this->Form->button(__('Submit')); ?>
								<?= $this->Form->end() ?>
								<!-- div class="users form">
								
									 <form id="loginform" class="form-horizontal" role="form" method="post" action="<?php //echo $this->Url->build(["controller" => "User", "action" => "login"]) ?>">
                    <label for="login-username">E-Mail/Login</label>
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="far fa-envelope"></i></span>
                        <input id="login-username" type="text" class="form-control" name="Email" value="" placeholder="E-Mail">
                    </div>
                    <label for="login-password">Password</label>
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="fas fa-key"></i></span>
                        <input id="login-password" type="password" class="form-control" name="Password" placeholder="">
                    </div> -->
                   <!--  <div style="margin-top:10px" class="form-group">
                        <div class="col-sm-push-1 col-sm-10 col-sm-pull-1 controls">
                            <button type="submit" id="btn-login" class="btn btn-primary btn-block"><i class="fas fa-sign-in-alt"></i> LOG IN</button>
                        </div>
                    </div> -->
								
								
									
								</div>

								<hr>

								<!-- <div class="row">
									<div class="col-lg-8">
										<b><a href="">Forgot password?</a></b>
									</div>
									<div class="col-lg-4 text-right">
										<button class="btn btn-action" type="submit">Sign in</button>
									</div>
								</div> -->
						</div>
					</div>

				</div>
				
			</article>
			<!-- /Article -->

		</div>
	</div>	<!-- /container -->
	
	
<?= $this->element('footer'); ?>