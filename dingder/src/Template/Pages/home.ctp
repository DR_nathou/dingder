<?= $this->element('header'); ?>
	
<!-- <body styles="background-image: url('https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Music-beam.svg/2000px-Music-beam.svg.png'); background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center;">
</body> -->
	<!-- Header -->
	<!-- <header id="head"> -->
		<!-- <div class="container"> -->
			<!-- <div class="row"> -->
				<!-- <h1 class="lead">AWESOME, CUSTOMIZABLE, FREE</h1> -->
				<!-- <img source="img/1.jpg"></img> -->
				<!-- <p class="tagline">PROGRESSUS: free business bootstrap template by <a href="http://www.gettemplate.com/?utm_source=progressus&amp;utm_medium=template&amp;utm_campaign=progressus">GetTemplate</a></p> -->
				<!-- <p><a class="btn btn-default btn-lg" role="button">MORE INFO</a> <a class="btn btn-action btn-lg" role="button">DOWNLOAD NOW</a></p> -->
			<!-- </div> -->
		<!-- </div> -->
	<!-- </header> -->
	<!-- /Header -->
	
	<!-- container -->
	<div class="container" >

		<div class="row">
			<!-- Article main content -->
			<article class="col-xs-12 maincontent" <?php if ($CurrentUser) echo 'style="display: none;"';?>>
			
				<header class="page-header">
				
					<h1 class="page-title"> <i class="fas fa-music"></i> Welcome to DINGder</h1>
				</header>
				
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				
					<div class="panel panel-default" >
					<!-- style="overflow:auto;" -->
						<div class="panel-body" >
						<!-- style="overflow:auto; height:350px;" -->
							<div class="row">
								<ul style="text-align:center">
									<br><h4 style="color:#1078B1">Come Join us and share you taste with the comunity.</h4>
									<br><h4 style="color:#1078B1">Add your favorites artists to your favorite</h4>
									<br><h4 style="color:#1078B1">Connect to other music lovers</h4>
									<br><h4 style="color:#1078B1">Dicover...</h4>
									<a href="/user/add" style="float:left;font-size:xx-large;color:#D33C44">Sign up here <i class="far fa-hand-rock"></i></a> 
									
									<a href="/user/login" style="float:right;font-size:xx-large;color:#1078B1">Login here <i class="fas fa-plug 3x"></i></a>
								</ul>
							</div>
						</div>	
					</div>

				</div>
				
			</article>
			<!-- /Article MOSTSTARRED-->
			<article class="col-xs-12 maincontent" <?php if (!$CurrentUser) echo 'style="display: none;"';?>>
			
				<header class="page-header" id="titlebar">
				
					<h1 class="page-title"> <i class="far fa-star 5x"></i> Most 3 artists starred</h1>
				</header>
				
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				
					<div class="panel panel-default" >
					<!-- style="overflow:auto;" -->
						<div class="panel-body" >
						<!-- style="overflow:auto; height:350px;" -->
						
							<div class="row">						
								<?php
									if(isset($mostStarred))
									{
										$i =0;
										$lastElement = end($mostStarred);
										
										
										foreach($mostStarred as $artist)
										{
											//PRETIER THAN UNREADABLE var_dump or print_r
											//print_r(json_encode($artist, JSON_PRETTY_PRINT));
																						
											$img = '/img/record.jpg';
											
											if(isset($artist['picture']) && $artist['picture'] != 'www' && $artist['picture'] != '')
												$img = $artist['picture'];
												
											if($i == 3)
											{
												echo '</div>
														<!-- <div class="row" style="width:inherit;">
															<div style="width:inherit;padding-top:5px;text-align:center;border-radius:10px;background-color:red;">
																<a href="#'.$artist['id'].'"><i class="far fa-caret-square-down 3x"></i></a>
															</div>
														</div> -->
													<div class="row">';
												$i = 0;
											}
											$i++;
											
											$tagList = array();
											
											foreach($artist['artist_id']['tag_list'] as $tag)
											{
												array_push($tagList, $tag['tag_id']['mb_name']);
											}
											$firstTag = '';
											
											if (count($tagList) > 0) $firstTag = $tagList[0];
											
											echo '<div id="'.$artist['id'].'" class="col-sm-4" style="padding:10px;"><div class="card" style="width: 18rem; text-align:center">
											  <img class="card-img-top" style="height:200px;width:256px;object-fit: cover;" src="'.$img.'" alt="artist image">
												  <div class="card-body">
													<h2 class="card-title">'.$artist['mb_name'].'</h2>
													<h4 class="card-title" >' .$artist['location'].'</h4>
													<a class="card-title" href="'.$artist['youtube'].'"> Youtube link <i class="fab fa-youtube"></i></a>
													
													<div class="tagtooltip card-title">' . $firstTag . '<span class="tagtooltiptext">'. join(" ", $tagList) .'</span></div>
													
													<a href="/favorite/add/gohome/'.$artist['id'].'" class="btn btn-primary" style="border-radius:10px;">-- I <i class="far fa-heart"></i> this --</a>
												  </div>
											  </div>
											</div>';
											if($artist == $lastElement) echo '</div>';
										}
									}
								?>
						</div>
					</div>

				</div>
				
			</article>
			
			<!-- /Article MOSTLIKED-->
			<article class="col-xs-12 maincontent" style="margin-bottom: 150px;">  <!-- style= "margin-top:-100px" -->
			
				<header class="page-header"  id="homelastliked">
				
					<h1 class="page-title"> <i class="far fa-star 5x"></i> Last 3 artists liked</h1>
				</header>
				
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				
					<div class="panel panel-default" >
					<!-- style="overflow:auto;" -->
						<div class="panel-body" >
						<!-- style="overflow:auto; height:350px;" -->
						
							<div class="row">						
								<?php
									if(isset($lastLiked))
									{
										$i =0;
										$lastElement = end($lastLiked);
										
										
										foreach($lastLiked as $artist)
										{
											//DEBUG PURPOSE
											//print_r(json_encode($artist, JSON_PRETTY_PRINT));
											
											$img = '/img/record.png';
											
											if(isset($artist['picture']) && $artist['picture'] != 'www' && $artist['picture'] != '')
												$img = $artist['picture'];
												
											if($i == 3)
											{
												echo '</div>
														<!-- <div class="row" style="width:inherit;">
															<div style="width:inherit;padding-top:5px;text-align:center;border-radius:10px;background-color:red;">
																<a href="#'.$artist['id'].'"><i class="far fa-caret-square-down 3x"></i></a>
															</div>
														</div> -->
													<div class="row">';
												$i = 0;
											}
											$i++;
											
											$tagList = array();
											
											foreach($artist['artist_id']['tag_list'] as $tag)
											{
												array_push($tagList, $tag['tag_id']['mb_name']);
											}
											$firstTag = '';
											
											if (count($tagList) > 0) $firstTag = $tagList[0];
											
											
											echo '<div id="'.$artist['id'].'" class="col-sm-4" style="padding:10px;"><div class="card" style="width: 18rem; text-align:center">
											  <img class="card-img-top" style="height:200px;width:256px;object-fit: cover;" src="'.$img.'" alt="artist image">
												  <div class="card-body">
													<h2 class="card-title">'.$artist['mb_name'].'</h2>
													<h4 class="card-title" >' .$artist['location'].'</h4>
													<a class="card-title" href="'.$artist['youtube'].'"> Youtube link <i class="fab fa-youtube"></i></a>
													
													<div class="tagtooltip card-title">' . $firstTag . '<span class="tagtooltiptext">'. join(" ", $tagList) .'</span></div>
													
													<a href="/favorite/add/gohome/'.$artist['id'].'" class="btn btn-primary" style="border-radius:10px;">-- I <i class="far fa-heart"></i> this --</a>
												  </div>
											  </div>
											</div>';
											if($artist == $lastElement) echo '</div>';
										}
									}
								?>
						</div>
					</div>

				</div>
				
			</article>
			<!-- /Article -->
		</div>
	</div>	<!-- /container -->			
				

	
<?= $this->element('footer'); ?>